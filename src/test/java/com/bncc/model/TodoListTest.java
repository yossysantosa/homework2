package com.bncc.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {
    @Test
    public void testGetTodoByIndex() {
        TodoList todo = new TodoList();
        String expectedTodo = "1. Bikin pr [NOT DONE]";

        todo.addTodo(1,"Bikin pr", "School");
        String actualTodo = todo.getTodoByIndex(0);

        assertEquals(expectedTodo, actualTodo);
    }

    @Test
    public void testGetTodoList(){
        TodoList todo = new TodoList();
        String expectedTodo = "1. Watching movies [NOT DONE]\n2. Learn java [NOT DONE]\n3. Call mom [NOT DONE]";

        todo.addTodo(1, "Watching movies", "Fun");
        todo.addTodo(2, "Learn java", "Urgent");
        todo.addTodo(3, "Call mom", "Urgent");
        String actualTodo = todo.getAllTodoList();

        assertEquals(expectedTodo, actualTodo);
    }

    @Test
    public void testUpdateTaskById(){
        TodoList todo = new TodoList();
        String expectedTodo = "1. Bikin PR [DONE]";

        todo.addTodo(1, "Bikin PR", "School");
        todo.markTaskDoneById(1);
        String actualTodo = todo.getAllTodoList();

        assertEquals(expectedTodo, actualTodo);
    }

    @Test
    public void testRemoveTaskById(){
        TodoList todo = new TodoList();
        String expectedTodo = "1. Watching movies [NOT DONE]\n3. Call mom [NOT DONE]";

        todo.addTodo(1, "Watching movies", "Fun");
        todo.addTodo(2, "Learn java", "Urgent");
        todo.addTodo(3, "Call mom", "Urgent");

        todo.removeTaskById(2);
        String actualTodo = todo.getAllTodoList();

        assertEquals(expectedTodo, actualTodo);
    }

    @Test
    public void testGetTodoListByCategory() {
        TodoList todo = new TodoList();
        String expectedTodo = "2. Learn java [NOT DONE]\n3. Call mom [NOT DONE]";

        todo.addTodo(1, "Watching movies", "Fun");
        todo.addTodo(2, "Learn java", "Urgent");
        todo.addTodo(3, "Call mom", "Urgent");

        String actualTodo = todo.getTodoListByCategory("Urgent");

        assertEquals(expectedTodo, actualTodo);
    }
}