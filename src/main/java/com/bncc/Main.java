package com.bncc;

import com.bncc.model.TodoList;
import com.bncc.view.Menu;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        TodoList todoList = new TodoList();
        Menu menu = new Menu();
        Scanner userInput = new Scanner(System.in);
        int chooseIndex;

        do {
            menu.displayMenu();
            System.out.println("Choose : ");
            chooseIndex = userInput.nextInt();

            switch (chooseIndex) {
                case (1) :
                    todoList.generateTodoList();
                    break;
                case (2) :
                    todoList.addTodoByInput();
                    break;
                case (3) :
                    todoList.displayAllTodoList();
                    break;
                case (4) :
                    todoList.selectTaskToUpdate();
                    break;
                case (5) :
                    todoList.inputTaskCategoryToDisplay();
                    break;
                case (6) :
                    todoList.selectTaskToRemove();
                    break;
                case (7) :
                    userInput.close();
                    System.exit(0);
                default:
                    System.out.println("Input between 1-7");
            }
        } while(true);
    }
}
